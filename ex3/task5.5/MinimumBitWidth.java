public class MinimumBitWidth
{
  public static void main(String[] args)
  {
    int numberOfValues = Integer.parseInt(args[0]);
    int numberOfBits = 0, number = 1;
    while(number < numberOfValues)
    {
      number*=2;
      numberOfBits++;
    }//while
    System.out.println(numberOfBits);
  }//main
}///class
