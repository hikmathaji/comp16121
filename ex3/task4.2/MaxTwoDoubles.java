public class MaxTwoDoubles
{
  public static void main(String[] args)
  {
    double firstDouble = Double.parseDouble(args[0]);
    double secondDouble = Double.parseDouble(args[1]);
    
    if(firstDouble > secondDouble ) 
      System.out.println(firstDouble + " is greater than "+ secondDouble);    
    else if(secondDouble > firstDouble)
      System.out.println(secondDouble + " is greater than "+ firstDouble);    
    else
      System.out.println(firstDouble + " is equal to "+ secondDouble);    

  }//main
}//class
