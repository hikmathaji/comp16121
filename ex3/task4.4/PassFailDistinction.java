public class PassFailDistinction
{
  public static void main(String[] args)
  {
    int mark = Integer.parseInt(args[0]);
    if(mark >= 50)
    {
      System.out.println("Pass");
      if(mark >=70)
        System.out.println("Distinction");
    }//if
      else   
    	System.out.println("Fail");
  }//main
}//class
