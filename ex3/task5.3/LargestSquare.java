public class LargestSquare
{
  public static void main(String[] args)
  {
    int requiredNumber = Integer.parseInt(args[0]);
    int number=0;
    while(number*number<=requiredNumber)
      number++;
    number--;//square of number is bigger than required one, so minus 1
    System.out.println(number);
  }//main
}///class
