public class MinimumTankSize
{
  public static void main(String[] args)
  {
    double requiredVolume = Double.parseDouble(args[0]);
    double size = 0.1;
    while(size*size*size < requiredVolume)
      size+=0.1;
    System.out.println("You need a tank of " + size + " per side to hold the volume " + 
                        requiredVolume + " cubic meters");
  } 
}
