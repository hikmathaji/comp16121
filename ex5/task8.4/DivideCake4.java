//Finds GCD of 4 numbers and prints out the portions for each number
//which can be calculated by number divided by GCD of all.

public class DivideCake4
{
  //GCD function, finds two elements of     
  public static int gcd(int firstNumber, int secondNumber)
  {
     while(firstNumber!=secondNumber)
    {
      if(firstNumber<secondNumber)
        secondNumber-=firstNumber;
      else
        firstNumber-=secondNumber;
    }//while
    return firstNumber;
  } 

  public static void main(String[] args)
  {
    int firstNumber = Integer.parseInt(args[0]);
    int secondNumber = Integer.parseInt(args[1]);
    int thirdNumber = Integer.parseInt(args[2]);
    int fourthNumber = Integer.parseInt(args[3]);

    //to find GCD of three values first we need to find
    //GCD of first and second, then third one and 
    //the previous result...

    //finding GCD of first and second values
    int gcdOfFirstTwo = gcd(firstNumber, secondNumber);
    //finding GCD of first and second and third values
    int gcdOfFirstThree = gcd(gcdOfFirstTwo, thirdNumber);
    //finding GCD of all values
    int gcdOfAll = gcd(gcdOfFirstThree, fourthNumber);
    
    //calculating the portions for each person
    int firstPortion = firstNumber/gcdOfAll;
    int secondPortion = secondNumber/gcdOfAll;
    int thirdPortion = thirdNumber/gcdOfAll;
    int fourthPortion = fourthNumber/gcdOfAll;
    int totalPortion = firstPortion+secondPortion+thirdPortion+fourthPortion;

    //printing out the gcd
    System.out.println("The GCD of "+firstNumber+" "+secondNumber+" "+
                        thirdNumber+" "+fourthNumber+" is " + gcdOfAll);
    
    //printing out the total portion
    System.out.println("So the cake should be divided into " + totalPortion);
    
    //printing out the portions
    System.out.println("The "+firstNumber+" year old gets "+firstPortion+
                        ", the "+secondNumber+" year old gets "+secondPortion+
                        ", the "+thirdNumber+" year old gets "+thirdPortion+
                        " and the "+fourthNumber+" year old gets "+
                        fourthPortion);
  }//main
}//class
