//Calculates Common Factors of numbers between 2 and 20 inclusively
//Uses separate methods for GCD and printing some lines
public class CommonFactorsTable
{
  public static void printASeparatingLine()
  {
    //printing out the first line
    System.out.print("|-----|");
    for (int column = 2; column < 20; column++)
      System.out.print("---");
    System.out.println("--|");
  }

  public static void printColumnNumbers()
  {
    //printing out the second line with column numbers
    System.out.print("|     |");
    for (int column = 2; column <= 20; column++)
    {
      if (column < 10)//if column is less than 10 put one more space
        System.out.print("  "+column);
      else
        System.out.print(" "+column);
    }
    //printing new line
    System.out.println();
  }

  public static int GCD(int multiple1OfGCD, int multiple2OfGCD)
  {
    while(multiple2OfGCD != multiple1OfGCD)
      if(multiple1OfGCD > multiple2OfGCD)
        multiple1OfGCD -= multiple2OfGCD;
      else
        multiple2OfGCD -= multiple1OfGCD;
    return multiple1OfGCD;
  }

  public static void main(String[] args)
  {
    printASeparatingLine();
    printColumnNumbers();
    printASeparatingLine();
    for (int row = 2; row <= 20; row++)
    {
      if(row < 10)
        System.out.print("|   " + row + " |");
      else
        System.out.print("|  " + row + " |");
      
      for (int column = 2; column <= 20; column++)
      {
        int gcd = GCD(row, column);
        //if gcd is 1 print --| if greater than 1 print --#  
        if(gcd == 1)
          System.out.print("--|");
        else
          System.out.print("--#");
      }
      //printing new line
      System.out.println();
    }
    //printing the last line
    printASeparatingLine();
   }//main
}///class
