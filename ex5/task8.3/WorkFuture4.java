//Prints work future for 4 people using a separate method
public class WorkFuture4
{
  public static void workFuture(String personName, int thisYear, int birthYear)
  {
    int yearsLeft = 68 - ( thisYear - birthYear); 

    System.out.println(personName + " has " + yearsLeft + " years to work");

    for(int year = yearsLeft-1; year >0; year-- )
    {
      //nextYear represents each year until retirement year
      int nextYear = thisYear + yearsLeft - year;
      System.out.println("In "+ nextYear + " " + personName +"  will have " + 
                          year + " years left to work");
    }//for

    System.out.println(personName + " will retire in " +(thisYear + yearsLeft));
  }

  public static void main(String[] args)
  {
    int thisYear = Integer.parseInt(args[0]);
    int firstBirthYear = Integer.parseInt(args[1]);
    int secondBirthYear = Integer.parseInt(args[2]);
    int thirdBirthYear = Integer.parseInt(args[3]);
    int fourthBirthYear = Integer.parseInt(args[4]);

    workFuture("First person", thisYear, firstBirthYear);
    workFuture("Second person", thisYear, secondBirthYear);
    workFuture("Thrid person", thisYear, thirdBirthYear);
    workFuture("Fourth person", thisYear, fourthBirthYear);
  }//main
}//class
