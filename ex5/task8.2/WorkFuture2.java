//Finds work future for 2 people by the same previous method. 
//No other methods used for this task
public class WorkFuture2
{
  public static void main(String[] args)
  {
    int thisYear = Integer.parseInt(args[0]);
    int firstBirthYear = Integer.parseInt(args[1]);
    int secondBirthYear = Integer.parseInt(args[2]);


    //yearsLeft = retirement year minus person's age
    int yearsLeftForPn1 = 68 - ( thisYear - firstBirthYear ); 
    int yearsLeftForPn2 = 68 - ( thisYear - secondBirthYear ); 

    //printing out the results of Pn1, first person
    System.out.println( "Pn1 has " +  yearsLeftForPn1 + " years to work" );

    for ( int year = yearsLeftForPn1 - 1; year > 0; year-- )
    {
      //nextYear represents each year until retirement year
      int nextYear = thisYear + yearsLeftForPn1 - year;
      System.out.println( "In "+ nextYear + " you will have " + 
                          year + " years left to work" );
    }//for

    System.out.println("You will retire in " + (thisYear + yearsLeftForPn1));

    //printing out the results of Pn2, second person
    System.out.println("Pn2 has "+ yearsLeftForPn2 + " years to work");

    for (int year = yearsLeftForPn2-1; year > 0; year-- )
    {
      //nextYear represents each year until retirement year
      int nextYear = thisYear + yearsLeftForPn2 - year;
      System.out.println("In " + nextYear + " you will have " + 
                          year + " years left to work");
    }//for

    System.out.println("You will retire in " + (thisYear + yearsLeftForPn2));
  }//main
}//class
