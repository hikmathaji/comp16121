public class CommonFactorsTable
{
  public static void main(String[] args)
  {
    //printing out the first line
    System.out.print("|-----|");
    for(int column = 2; column < 20; column++)
      System.out.print("---");
    System.out.println("--|");
    
    //printing out the second line with column numbers
    System.out.print("|     |");
    for(int column = 2; column <= 20; column++)
    {
      if(column<10)//if column is less than 10 put one more space
        System.out.print("  "+column);
      else
        System.out.print(" "+column);
    }
    //printing new line
    System.out.println();
    
    //printing the third line
    System.out.print("|-----|");
    //printing the column numbers
    for(int column = 2; column < 20; column++)
      System.out.print("---");
    System.out.println("--|");
  
    for(int row = 2; row<=20; row++)
    {
      if(row<10)
        System.out.print("|   "+row+" |");
      else
        System.out.print("|  "+row+" |");
      
      for(int column = 2; column<=20; column++)
      {
        int multiple1OfGCD = row;
        int multiple2OfGCD = column;
        while(multiple2OfGCD != multiple1OfGCD)
          if(multiple1OfGCD > multiple2OfGCD)
            multiple1OfGCD-=multiple2OfGCD;
          else
            multiple2OfGCD-=multiple1OfGCD;
        //if GCD is 1 print --| if greater than 1 print --#  
        if(multiple1OfGCD == 1)
          System.out.print("--|");
        else
          System.out.print("--#");
      }
      //printing new line
      System.out.println();
    }
    //printing the last line
    System.out.print("|-----|");
    for(int column = 2; column < 20; column++)
      System.out.print("---");
    System.out.println("--|");
   }//main
}///class
