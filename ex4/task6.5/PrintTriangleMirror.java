public class PrintTriangleMirror
{
  public static void main(String[] args)
  {
    //getting size from the command line
    int size = Integer.parseInt(args[0]);
    
    for(int row = 0; row < size; row++)
    {
      for(int column = 0; column < size; column++)
      {
        if(row>column)//means these are empty tiles
          System.out.print("   ");//printing out the empty tiles
        else
          System.out.print("[_]");//printing out the tiles
      }
      System.out.println();//printing the new line
    }
  }//main
}///class
