public class PrintHoledRectangle
{
  public static void main(String[] args)
  {
    //getting width and height of the rectangle
    int width = Integer.parseInt(args[0]);
    width = (width / 2) * 2 + 1
    int height = Integer.parseInt(args[1]);
    height = (height / 2) * 2 + 1
    for(int row = 0; row < height; row++)
    {
      for(int column = 0; column < width; column++)
      {
        if((row*2+1)==height&&(column*2+1)==width)
          //printing the hole
          System.out.print("   ");
        else
          //printing the rectangle tiles
          System.out.print("[_]");
      }//for
      System.out.println();
    }//for
  }//main
}///class
