public class WorkFuture
{
  public static void main(String[] args)
  {
    int thisYear = Integer.parseInt(args[0]);
    int birthYear = Integer.parseInt(args[1]);

    //yearsLeft = retirement year minus her age
    int yearsLeft = 68 - ( thisYear - birthYear); 

    System.out.println("You have "+ yearsLeft +" years to work");

    for(int year = yearsLeft-1; year >0; year-- )
    {
      //nextYear represents each year until retirement year
      int nextYear = thisYear+yearsLeft-year;
      System.out.println("In "+ nextYear +" you will have " + 
                          year + " years left to work");
    }//for

    System.out.println("You will retire in " + (thisYear + yearsLeft));
  }//main
}//class