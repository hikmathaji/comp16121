public class DivideCake3
{
  public static void main(String[] args)
  {
    int firstNumber = Integer.parseInt(args[0]);
    int secondNumber = Integer.parseInt(args[1]);
    int thirdNumber = Integer.parseInt(args[2]);

    //to find GCD of three values first we need to find
    //GCD of first and second, then third one and 
    //the previous result...

    //finding GCD of first and second values
    while(firstNumber!=secondNumber)
    {
      if(firstNumber>secondNumber)
        firstNumber-=secondNumber;
      else
        secondNumber-=firstNumber;
    }//while
    
    //as firstNumber = secondNumber
    int gcd = firstNumber;

    while(gcd!=thirdNumber)
    {
      if(gcd<thirdNumber)
        thirdNumber-=gcd;
      else
        gcd-=thirdNumber;
    }//while

    //now gcd is GCD of all three values    
    //reassigning the original values
    firstNumber = Integer.parseInt(args[0]);
    secondNumber = Integer.parseInt(args[1]);
    thirdNumber = Integer.parseInt(args[2]);

    //calculating the portions for each person
    int firstPortion = firstNumber/gcd;
    int secondPortion = secondNumber/gcd;
    int thirdPortion = thirdNumber/gcd;
    int totalPortion = firstPortion + secondPortion + thirdPortion;

    //printing out the gcd
    System.out.println("The GCD of "+firstNumber+" "+secondNumber+" "+
                        thirdNumber+" is " + gcd);
    
    //printing out the total portion
    System.out.println("So the cake should be divided into " + totalPortion);
    
    //printing out the portions
    System.out.println("The "+firstNumber+" year old gets "+firstPortion+
                        ", the "+secondNumber+" year old gets "+secondPortion+
                        " and the "+thirdNumber+" year old gets "+thirdPortion);
  }//main
}//class