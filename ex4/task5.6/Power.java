public class Power
{
  public static void main(String[] args)
  {
  	//getting the first and second numbers from command line
	int firstNumber = Integer.parseInt(args[0]);
	int secondNumber = Integer.parseInt(args[1]);

	//initializing the result as 1 
	int result = 1;
	while(secondNumber>0)
	{
	  secondNumber=secondNumber-1;
	  result = result * firstNumber;
    }//while

    //printing the result
	System.out.println(result);
  }//main
}//Class