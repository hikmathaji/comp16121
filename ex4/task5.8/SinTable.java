public class SinTable
{
  public static void main(String[] args)
  {
    //getting the command line arguments
    int startingPoint = Integer.parseInt(args[0]);
    int increment = Integer.parseInt(args[1]);
    int endingPoint = Integer.parseInt(args[2]);
    //printing the first line
    System.out.println("----------------------------------------------");
    //printing the information about the input arguments
    System.out.println("| Sin table from "+ startingPoint + " to " + 
                      endingPoint + " in steps of "+increment);
    //printing the third line
    System.out.println("----------------------------------------------");
    //finding and printing the sins
    for(int degree = startingPoint; degree<=endingPoint; degree+=increment)
      System.out.println("| sin(" + degree + ") = " +
                         Math.sin(Math.toRadians(degree)));
    //printing the last line
    System.out.println("----------------------------------------------");
  }//main
}//class