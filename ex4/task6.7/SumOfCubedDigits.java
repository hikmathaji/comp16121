public class SumOfCubedDigits
{
  public static void main(String[] args)
  {
    for(int firstNumber = 1; firstNumber < 10; firstNumber++)
    {
      for(int secondNumber = 0; secondNumber < 10; secondNumber++)
      {
        for(int thirdNumber = 0; thirdNumber < 10; thirdNumber++)
        {
          //generating number from first, second and third numbers
          int generatedNumber = firstNumber*100+secondNumber*10+thirdNumber;
          //finding sum of cubes
          int sumOfCubes = firstNumber*firstNumber*firstNumber + 
              secondNumber*secondNumber*secondNumber + 
              thirdNumber*thirdNumber*thirdNumber;
          //if the number generated equal to sum of cubes print the number
          if(generatedNumber==sumOfCubes)
            System.out.println(generatedNumber);
        }//inner for 
      }//middle for
    }//outer for
  }//main
}///class
