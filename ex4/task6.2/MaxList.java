public class MaxList
{
  public static void main(String[] args)
  {
    //intializing maximum by first number
    int max = Integer.parseInt(args[0]);
    
    //starting from the second number
    for(int index = 1; index<args.length; index++)
    {
      //just greater because task requires least index for same values
      if(max < Integer.parseInt(args[index]))
      {
        max = Integer.parseInt(args[index]);
      }//if
    }//for

    //printing out the maximum value
    System.out.println("Maximum is "+max);
  }//main
}//class