public class Variance
{
  public static void main(String[] args)
  {
    int sumOfAll = 0;//variable initialization
    for(int index = 0; index<args.length; index++)
    	sumOfAll += Integer.parseInt(args[index]);

    double mean = sumOfAll/args.length;
    double sumOfSquaresOfDeviations = 0;//variable initialization
    
    for(int index = 0; index<args.length; index++)
    {
    	double deviation = Integer.parseInt(args[index])-mean//the deviation
    	sumOfSquaresOfDeviations+=deviation*deviation;
		}
		double variance = sumOfSquaresOfDeviations/args.length;//the variance

    //printing out the results
		System.out.println("The mean average is "+mean);
		System.out.println("The variance is "+variance);
  }//main
}//class