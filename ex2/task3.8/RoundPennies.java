public class RoundPennies
{
  public static void main(String[] args)
  {
    int amountOfPennies = Integer.parseInt(args[0]);
    if(amountOfPennies < 0 )
      amountOfPennies = 0;
    int amountOfPounds = amountOfPennies / 100;
    if(amountOfPennies - 100*amountOfPounds >= 50)
      amountOfPounds = amountOfPounds + 1;
    System.out.println("Amount of pounds: " + amountOfPounds);
  }
}
