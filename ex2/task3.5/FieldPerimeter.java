public class FieldPerimeter
{
  public static void main(String[] args)
  {
    int width = Integer.parseInt(args[0]);
    int height = Integer.parseInt(args[1]);
    int perimeter = 2*(width+height);
    System.out.println("The perimeter is  " + perimeter);
  } 
}
