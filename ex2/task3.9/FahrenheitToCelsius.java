public class FahrenheitToCelsius
{
  public static void main(String[] args)
  {
    double fahrenheitValue = Integer.parseInt(args[0]);
    double celciusValue = (5*fahrenheitValue - 160)/9.0;
    
    System.out.println("Temperature " + fahrenheitValue + " Fahrenheit in Celcius is " + celciusValue);
  }
}
