public class YearsBeforeRetirement
{
  public static void main(String[] args)
  {
    int myAgeNow = Integer.parseInt(args[0]);
    int myRetirementAge = 68; 
    int yearsLeftBeforeRetirement = myRetirementAge - myAgeNow;
    System.out.println("My age now is : " + myAgeNow);
    System.out.println("I will retire at the age of " + myRetirementAge);
    System.out.println("Years left working is  " + yearsLeftBeforeRetirement);
  } 
}
