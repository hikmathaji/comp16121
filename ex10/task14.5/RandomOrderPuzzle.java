import java.util.Scanner;
import java.io.File;

/**
 *  An interactive puzzle that invites users to rearrange lines from randomized
 *  text in order to get original order.
 *
 *  @author Hikmat Hajiyev
 */
public class RandomOrderPuzzle
{
  //initial array size
  private static final int INITIAL_ARRAY_SIZE = 10;
  //the factor to increase the array size
  private static final int ARRAY_RESIZE_FACTOR = 2;
  
  private int noOfLines;
  private String[] puzzleInOriginalOrder;
  private String[] puzzleInRandomOrder;
  public RandomOrderPuzzle(Scanner fileScanner)
  {
    puzzleInOriginalOrder = new String[INITIAL_ARRAY_SIZE];
    noOfLines = 0;
    while (fileScanner.hasNextLine())
    {
      //Create longer array, copy all elements to it if current array is small
      if (noOfLines == puzzleInOriginalOrder.length)
      {
        String[] biggerArray 
          = new String[puzzleInOriginalOrder.length * ARRAY_RESIZE_FACTOR];
        //copy all the elements
        for (int index = 0; index < puzzleInOriginalOrder.length; index++)
          biggerArray[index] = puzzleInOriginalOrder[index];
        puzzleInOriginalOrder = biggerArray;
      }//if

      puzzleInOriginalOrder[noOfLines] = fileScanner.nextLine();
      noOfLines++;
    }//while
    puzzleInRandomOrder = new String[noOfLines];
    for (int index = 0; index < noOfLines; index++)
      puzzleInRandomOrder[index] = puzzleInOriginalOrder[index];
     randomizeStringArrayOrder(puzzleInRandomOrder);
  }//RandomOrderPuzzle

  public boolean isSorted()
  {
    for (int index = 0; index < noOfLines; index++)
    {
      if(!puzzleInOriginalOrder[index].equals(puzzleInRandomOrder[index]))
        return false;
    }//for
    return true;
  }//isSorted

  public void swapLine(int userChoice)
  {
    String userChosenString = puzzleInRandomOrder[userChoice];
    puzzleInRandomOrder[userChoice] = puzzleInRandomOrder[noOfLines-1];
    puzzleInRandomOrder[noOfLines-1] = userChosenString;
  }//swapLine

  public String toString()
  {
    String wholeString = "";
    for (int index = 0; index < noOfLines; index++)
      wholeString += index + "    " + puzzleInRandomOrder[index] + "\n";
    return wholeString;
  }//toString

  private void randomizeStringArrayOrder(String[] anArray)
  {
    for(int itemsRemaining = anArray.length; itemsRemaining > 0; 
        itemsRemaining--)
    {
      int anIndex = (int) (Math.random() * itemsRemaining);
      String itemAtAnIndex = anArray[anIndex];
      anArray[anIndex] = anArray[anArray.length -1];
      anArray[anArray.length - 1] = itemAtAnIndex;
    }//for
  }//randomizeStringArrayOrder



  public static void main(String[] args) throws Exception
  {
    Scanner fileScanner = new Scanner(new File(args[0]));
    RandomOrderPuzzle puzzle = new RandomOrderPuzzle(fileScanner);

    Scanner inputScanner = new Scanner(System.in);
    System.out.println(puzzle);
    int moveCount = 0;
    while (! puzzle.isSorted())
    {
      System.out.print("Enter a line number to swap with the last one: ");
      puzzle.swapLine(inputScanner.nextInt());
      System.out.println(puzzle);
      moveCount++;
    }//while
    System.out.println("Game over in " + moveCount + " moves.");
  }//main
}


