import java.util.Scanner;

/**
 *  Lists students coursework marks and produces a report. The scores entered 
 *  by the user via standart input.
 *
 *  @author Hikmat Hajiyev
 */
public class MarkAnalysis
{
  public static void sort(Student[] students)
  { 
    boolean anyChanges;
    int myLenght = students.length;
    do
    {
      anyChanges = false;
      for(int arrayIndex = 0; arrayIndex < myLenght-1; arrayIndex++)
      {
        if(students[arrayIndex].compareTo(students[arrayIndex+1]))
        {
          Student oldValueOfFirst = students[arrayIndex];
          students[arrayIndex] = students[arrayIndex+1];
          students[arrayIndex+1] = oldValueOfFirst;
          anyChanges = true;
        }//if
      }//for
      myLenght--;
    }while(anyChanges);
  }//sort

  public static void main(String[] args)
  {
    Scanner marksScanner = new Scanner(System.in);
    System.out.println("Enter the number of marks: ");
    int numberOfStudents = 0;
    numberOfStudents = marksScanner.nextInt();
    while(numberOfStudents < 1)
    {
      System.out.println("Wrong number. Try again");
      numberOfStudents = marksScanner.nextInt();
    }//while
    marksScanner.nextLine();
    Student[] students = new Student[numberOfStudents];
    int sumOfMarks = 0;
    for(int studentIndex = 0; studentIndex<numberOfStudents; studentIndex++)
    {
      System.out.print("Enter the name of a student #" 
                        + (studentIndex + 1) + ": ");
      String studentName = marksScanner.nextLine();

      System.out.print("Enter the mark for \'" + studentName + "\': ");
      int studentMark = marksScanner.nextInt();
      marksScanner.nextLine();
      students[studentIndex] = new Student(studentName, studentMark);
      sumOfMarks += studentMark;
    }
    double meanMark = (double)sumOfMarks / (double)numberOfStudents;
    //sorting the students marks
    sort(students);
    //printing out the result
    System.out.println();
    System.out.println("The mean mark is: \t" + meanMark);
    System.out.println("The minimum mark is: \t" + students[0].getMark());
    System.out.println("The maximum mark is: \t" 
                      + students[numberOfStudents-1].getMark());
    System.out.println(); 
    System.out.println("Person and Score  | difference from mean");
    for(Student student: students)
    {
      System.out.print(student.toString());
      double markToPrint =  (double)student.getMark() - meanMark;
      System.out.printf("| %6.2f%n", markToPrint);
    }//for
  }//main 
}//MarkAnalysis class
