import java.util.Scanner;
/**
 *  Helper Class for MarkAnalysis, represents students
 *
 *  @author Hikmat Hajiyev
 */
public class Student
{
  private final String name;
  private int mark;
  public Student(String newName, int newMark)
  {
  	name = newName;
  	mark = newMark;
  }//constructor
  public String getName()
  {
  	return name;
  }//getName
  public int getMark()
  {
  	return mark;
  }//getMark
  public boolean compareTo(Student otherStudent)
  {
  	return otherStudent.mark < mark;
  }//compareTo
  public String toString()
  {
  	return String.format("%-10s got %3d", name, mark);
  }//toString
}//MarkAnalysis class
