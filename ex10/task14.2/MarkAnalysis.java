import java.util.Scanner;

/**
 *  Lists students coursework marks and produces a report. The scores entered 
 *  by the user via standart input.
 *
 *  @author Hikmat Hajiyev
 */
public class MarkAnalysis
{
  public static void main(String[] args)
  {
    Scanner marksScanner = new Scanner(System.in);
    System.out.println("Enter the number of marks: ");
    int numberOfMarks = 0;
    numberOfMarks = marksScanner.nextInt();
    while(numberOfMarks < 1)
    {
      System.out.println("Wrong number. Try again");
      numberOfMarks = marksScanner.nextInt();
    }//while
    int[] marks = new int[numberOfMarks];
    int sumOfMarks = 0, minimumMark = 2147483647, maximumMark = -2147483648;
    for(int markIndex = 0; markIndex<numberOfMarks; markIndex++)
    {
      System.out.print("Enter mark #" + (markIndex + 1) + ": ");
      marks[markIndex] = marksScanner.nextInt();
      sumOfMarks += marks[markIndex];
      if(marks[markIndex] < minimumMark)
        minimumMark = marks[markIndex];
      if(marks[markIndex] > maximumMark)
        maximumMark = marks[markIndex];
    }//for
    double meanMark = (double)sumOfMarks / (double)numberOfMarks;
    System.out.println();
    System.out.println("The mean mark is: \t" + meanMark);
    System.out.println("The minimum mark is: \t" + minimumMark);
    System.out.println("The maximum mark is: \t" + maximumMark);
    System.out.println(); 
    System.out.println("Person | Score | difference from mean");
    for(int markIndex = 0; markIndex<numberOfMarks; markIndex++)
      System.out.printf("%6d | %5d | %6.2f%n", markIndex, marks[markIndex], 
                        marks[markIndex] - meanMark);
  }//main 
}//MarkAnalysis class
