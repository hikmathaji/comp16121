class Triangle
{
	public Point A;
	public Point B;
	public Point C;
	public double sideA;
	public double sideB;
	public double sideC;
	public Triangle(Point pointA, Point pointB, Point pointC)
	{
		A = pointA;
		B = pointB;
		C = pointC;
		sideA = A.distance(B);
		sideB = B.distance(C);
		sideC = C.distance(A);
	}//constructor
	public double perimeter()
	{
		return sideA + sideB + sideC;
	}//perimeter
	public double area()
	{
		double s = this.perimeter()/2.0;
		return Math.sqrt(s * (s - sideA) * (s - sideB) * (s - sideC));
	}//area

	public String toString()
	{
		return "Triangle(" + A.toString() + ", " + B.toString() + ", " + C.toString() + ")";
	}//toString
	
	public Triangle shift(double xShift, double yShift)
	{
		Point secondA = A.shift(xShift, yShift);
		Point secondB = B.shift(xShift, yShift);
		Point secondC = C.shift(xShift, yShift);
		return new Triangle(secondA, secondB, secondC);
	}//shift
}//class Triangle