//User choose one of four shapes and shift them on a cartesian plane
//all the inputs by the user are standart
//using five other classes Point,Circle, Triangle, Rectangle and Polygon

import java.util.Scanner;

public class ShapeShift
{
	public static Scanner inputScanner = new Scanner(System.in);
	
	private static Point inputPoint(String prompt)
	{
		System.out.print(prompt);
		double x = inputScanner.nextDouble();
		double y = inputScanner.nextDouble();
		return new Point(x, y);
	}//input Point

	private static double xShift, yShift;


	private static void inputXYShifts()
	{
		System.out.print("Enter the offset as X Y: ");
		xShift = inputScanner.nextDouble();
		yShift = inputScanner.nextDouble();
	}

	public static void main(String[] args)
	{
		System.out.println("Choose circle(1), triangle (2), rectangle(3), polygon(4)");
		int shapeChoice = inputScanner.nextInt();
		
		switch(shapeChoice)
		{
			case 1:
			Point centre = inputPoint("Enter the centre as X Y : ");
			System.out.print("Enter the radius: ");
			double radius = inputScanner.nextDouble();
			Circle originalCircle = new Circle(centre, radius);
			inputXYShifts();
			Circle shiftedCircle = originalCircle.shift(xShift, yShift);
			System.out.println();
			System.out.println(originalCircle);
			System.out.println("has area " + originalCircle.area()
								+ ", perimeter " + originalCircle.perimeter());
			System.out.println("and when shifted by X offset " + xShift
								+ " and Y offset " + yShift + ", gives");
			System.out.println(shiftedCircle);
			break;
			case 2:
			Point pointA = inputPoint("Enter point A as X Y: ");
			Point pointB = inputPoint("Enter point B as X Y: ");
			Point pointC = inputPoint("Enter point C as X Y: ");
			Triangle originalTriangle = new Triangle(pointA, pointB, pointC);
			inputXYShifts();
			Triangle shiftedTriangle = originalTriangle.shift(xShift, yShift);
			System.out.println();
			System.out.println(originalTriangle);
			System.out.println("has area " + originalTriangle.area()
								+ ", perimeter " + originalTriangle.perimeter());
			System.out.println("and when shifted by X offset " + xShift
								+ " and Y offset " + yShift + ", gives");
			System.out.println(shiftedTriangle);
			break;

			case 3:
			Point diag1End1 = inputPoint("Enter one corner as X Y: ");
			Point diag1End2 = inputPoint("Enter opposite corner as X Y: ");
			Rectangle originalRectangle = new Rectangle(diag1End1, diag1End2);
			inputXYShifts();
			Rectangle shiftedRectangle = originalRectangle.shift(xShift, yShift);
			System.out.println();
			System.out.println(originalRectangle);
			System.out.println("has area " + originalRectangle.area()
								+ ", perimeter " + originalRectangle.perimeter());
			System.out.println("and when shifted by X offset " + xShift
								+ " and Y offset " + yShift + ", gives");
			System.out.println(shiftedRectangle);
			break;

			case 4:
			System.out.println("Enter number of sides: ");
			int numberOfSides = inputScanner.nextInt();
			Point[] points = new Point[numberOfSides];
			for(int currentPointIndex=0; currentPointIndex<numberOfSides; currentPointIndex++)
			{
				points[currentPointIndex] = inputPoint("Enter Point" + (currentPointIndex + 1) + " as X Y: ");
			}

		
			Polygon originalPolygon = new Polygon(numberOfSides, points);
			inputXYShifts();
			Polygon shiftedPolygon = originalPolygon.shift(xShift, yShift);
			System.out.println();
			System.out.println(originalPolygon);
			System.out.println("has area " + originalPolygon.area()
								+ ", perimeter " + originalPolygon.perimeter());
			System.out.println("and when shifted by X offset " + xShift
								+ " and Y offset " + yShift + ", gives");
			System.out.println(shiftedPolygon);
			break;

			default:
			System.out.println("That wasn't 1, 2, 3 or 4!");
			break;
		}//switch
	}//main
}//class ShapeShift