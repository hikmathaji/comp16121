class Polygon
{
	public Point[] points;
	public int numberOfSides;
	public double[] sides;
	public Polygon(int number, Point[] polygonPoints)
	{
		points = polygonPoints;
		numberOfSides = number;
		sides = new double[numberOfSides];
		for(int currentPointIndex=0; currentPointIndex<numberOfSides-1; currentPointIndex++)
		{
			sides[currentPointIndex] = points[currentPointIndex].distance(points[currentPointIndex+1]);
		}//for
		sides[numberOfSides-1] = points[numberOfSides-1].distance(points[0]);
	}//constructor

	public double perimeter()
	{
		double perimeterByNow = 0;//perimeter
		for(int currentPointIndex=0; currentPointIndex<numberOfSides; currentPointIndex++)
		{
			perimeterByNow += sides[currentPointIndex];
		}//for
		return perimeterByNow;
	}//perimeter
	public double area()
	{
		double areaByNow = 0;
		for(int currentPointIndex=0; currentPointIndex<numberOfSides-1; currentPointIndex++)
		{
			areaByNow += (points[currentPointIndex].x + points[currentPointIndex+1].x) * (points[currentPointIndex].y - points[currentPointIndex+1].y);
		}//for
		areaByNow += (points[numberOfSides-1].x + points[0].x) * (points[numberOfSides-1].y - points[0].y);
		return Math.abs(areaByNow/2.0);
	}//area

	public String toString()
	{
		String output = "Polygon(";
		for(int currentPointIndex=0; currentPointIndex<numberOfSides-1; currentPointIndex++)
		{
			output+=points[currentPointIndex].toString()+", ";
		}//for
		output+=points[numberOfSides-1].toString()+")";
		return output;

	}//toString
	public Polygon shift(double xShift, double yShift)
	{
		Point[] otherPoints =  new Point[numberOfSides];
		for(int currentPointIndex=0; currentPointIndex<numberOfSides; currentPointIndex++)
		{
			otherPoints[currentPointIndex]=points[currentPointIndex].shift(xShift, yShift);
		}//for

		return new Polygon(numberOfSides, otherPoints);
	}//shift
}//class polygon