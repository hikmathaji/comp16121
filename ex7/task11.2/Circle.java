class Circle
{
	public Point centre;
	public double radius;

	public Circle(Point cen, double rad)
	{
		centre = cen;
		radius = rad;
	}//constructor

	public double area()
	{
		return Math.PI * radius * radius;
	}//area

	public double perimeter()
	{
		return 4.0 * Math.PI * radius;
	}//perimeter

	public String toString()
	{
		return "Circle(" + centre.toString() + ", " + radius + ")";
	}//toString

	public Circle shift(double xShift, double yShift)
	{
		Point secondCentre = centre.shift(xShift, yShift);
		return new Circle(secondCentre, radius);
	}//shift
}//class Circle