class Rectangle
{
	public Point A;
	public Point B;
	public Point C;
	public Point D;
	public double sideA;
	public double sideB;
	public double sideC;
	public double sideD;
	public Rectangle(Point corner, Point oppositeCorner)
	{
		A = corner;
		B = new Point(corner.x, oppositeCorner.y);
		C = oppositeCorner;
		D = new Point(oppositeCorner.x, corner.y);
		sideA = A.distance(B);
		sideB = B.distance(C);
		sideC = C.distance(D);
		sideD = D.distance(A);
	}//constructor
	public double perimeter()
	{
		return sideA + sideB + sideC + sideD;
	}//perimeter
	public double area()
	{
		return sideA*sideB;
	}//area

	public String toString()
	{
		return "Rectangle(" + A.toString() + ", " + B.toString() + ", " + C.toString() + ", " + D.toString() +")";
	}//toString
	public Rectangle shift(double xShift, double yShift)
	{
		Point secondA = A.shift(xShift, yShift);
		Point secondC = C.shift(xShift, yShift);
		return new Rectangle(secondA, secondC);
	}//shift
}//class Rectangle
