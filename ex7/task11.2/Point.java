public class Point
{
	public double x;
	public double y;

	public Point(double X, double Y)
	{
		x = X;
		y = Y;
	}
	public double distance(Point otherPoint)
	{
		return Math.sqrt((x - otherPoint.x) * (x - otherPoint.x) + (y - otherPoint.y) * (y - otherPoint.y));
	}
	public Point shift(double xShift, double yShift)
	{
		return new Point(x + xShift, y + yShift);
	}
	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}

}