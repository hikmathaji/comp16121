//adds two polynomial which coefficients are given from the command line
//
public class AddQuadPoly
{
  public static void main(String[] args)
  {
  	//getting the coefficients
  	double firstCoefficientofFirst = Double.parseDouble(args[0]);
  	double secondCoefficientofFirst = Double.parseDouble(args[1]);
  	double thirdCoefficientofFirst = Double.parseDouble(args[2]);
  	QuadPoly firstPoly = new QuadPoly(firstCoefficientofFirst, secondCoefficientofFirst, thirdCoefficientofFirst);
    QuadPoly secondPoly = new QuadPoly(Double.parseDouble(args[3]),Double.parseDouble(args[4]),Double.parseDouble(args[5]));
  	QuadPoly thirdPoly = firstPoly.newQuadPoly(Double.parseDouble(args[3]),Double.parseDouble(args[4]),Double.parseDouble(args[5]));
  	//printing out the result
  	System.out.println("Polynomial :  "+firstPoly.toString());
  	System.out.println("added to   :  "+secondPoly.toString());
  	System.out.println("results in :  "+thirdPoly.toString());
  }
}