class QuadPoly
	{
		public double firstCoefficient;
		public double secondCoefficient;
		public double thirdCoefficient;
		public QuadPoly(double first, double second, double third)
		{
			firstCoefficient = first;
			secondCoefficient = second;
			thirdCoefficient = third;
		}//constructor
		//creates new polynomial, an instance method
		public QuadPoly newQuadPoly(double first, double second, double third)
		{
			QuadPoly tempQuadPoly = new QuadPoly(first, second, third);
			tempQuadPoly.addQuadPoly(this, tempQuadPoly);
			return tempQuadPoly;
		}//newQuadPoly
		//add two polynomials
		public void addQuadPoly(QuadPoly firstPoly, QuadPoly secondPoly)
		{
			firstCoefficient = firstPoly.firstCoefficient + secondPoly.firstCoefficient;
			secondCoefficient = firstPoly.secondCoefficient + secondPoly.secondCoefficient;
			thirdCoefficient = firstPoly.thirdCoefficient + secondPoly.thirdCoefficient;
		}//addQuadPoly

		//checks if the two polynomials are equal
		public boolean equalQuadPoly(QuadPoly secondPoly)
		{
			if(firstCoefficient==secondPoly.firstCoefficient&&secondCoefficient==secondCoefficient&&thirdCoefficient==secondPoly.thirdCoefficient)
				return true;
			else
				return false;
		}//equalQuadPoly
		//compares two polynomials
		public boolean compareQuadPoly(QuadPoly secondPoly)
		{
			if(firstCoefficient > secondPoly.firstCoefficient)
				return true;
			else if(firstCoefficient < secondPoly.firstCoefficient)
				return false;

			if(secondCoefficient > secondPoly.secondCoefficient)
				return true;
			else if(secondCoefficient < secondPoly.secondCoefficient)
				return false;

			if(thirdCoefficient > secondPoly.thirdCoefficient)
				return true;
			return false;
		}//compareQuadPoly
		public String toString()
		{
			return firstCoefficient+"x^2+"+secondCoefficient+"x+"+thirdCoefficient;
		}//toString
	}//class