public class QuadPoly
{
  public double firstCoefficient, secondCoefficient, thirdCoefficient;
  public QuadPoly(double first, double second, double third)
  {
    firstCoefficient = first;
    secondCoefficient = second;
    thirdCoefficient = third;
  }//constructor
public void addQuadPoly(QuadPoly firstPoly, QuadPoly secondPoly)
{
	firstCoefficient = firstPoly.firstCoefficient 
		+ secondPoly.firstCoefficient;
	secondCoefficient = firstPoly.secondCoefficient
		+ secondPoly.secondCoefficient;
	thirdCoefficient = firstPoly.thirdCoefficient 
		+ secondPoly.thirdCoefficient;
}//addQuadPoly
}