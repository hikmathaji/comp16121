//Comparing two quadratic polynomial and telling which one is greater, smaller
//or they are equal. The cooefficients are entered from the command line

public class CompareQuadPoly
{
  public static void main(String[] args)
  {
  	//getting the coefficients from command line
  	double firstCoefficientofFirst = Double.parseDouble(args[0]);
  	double secondCoefficientofFirst = Double.parseDouble(args[1]);
  	double thirdCoefficientofFirst = Double.parseDouble(args[2]);
  	QuadPoly firstPoly = new QuadPoly(firstCoefficientofFirst, 
  		secondCoefficientofFirst, thirdCoefficientofFirst);
  	QuadPoly secondPoly = new QuadPoly(Double.parseDouble(args[3]), 
  		Double.parseDouble(args[4]), Double.parseDouble(args[5]));

  	//printing out the result
  	System.out.println("Polynomial:  	" + firstPoly.firstCoefficient + "x^2 + "
  										 + firstPoly.secondCoefficient + "x + " 
  										 + firstPoly.thirdCoefficient);
  	//checking if the two polynomial are same
  	if(firstPoly.equalQuadPoly(secondPoly))
  		System.out.print("is the same as:		");
  	else
  	{
  		//checking which one is greater
  		if(firstPoly.compareQuadPoly(secondPoly))
  		{
  			System.out.print("is smaller than  ");
  		}//if
  		else
  		{
  			System.out.print("is greater than  ");
  		}//else
  	}//else
  	System.out.println(secondPoly.firstCoefficient + "x^2 + " 
  										+ secondPoly.secondCoefficient + "x + " 
  										+ secondPoly.thirdCoefficient);
  }//main
}//class CompareQuadPoly