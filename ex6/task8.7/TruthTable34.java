// generating truth table for p1, p2 and p3 for the given expressions
public class TruthTable34
{
	public static boolean p1(boolean a, boolean b, boolean c, boolean d)
	{
		return ((a || b) && c) || ((b || c) && d) && (a || d);
	}//p1

	public static boolean p2(boolean a, boolean b, boolean c, boolean d)
	{
		return a && c || b && d || c && d;
	}//p2

	public static boolean p3(boolean a, boolean b, boolean c, boolean d)
	{
		return (b || c) && (c || d) && (a || d);
	}//p3

	public static String printRowItem(boolean a)
	{
		return a?" true  |":" false |";
	}//printRowItem

	public static void printRow(boolean a, boolean b, boolean c, boolean d)
	{
		System.out.println("|" + printRowItem(a) + printRowItem(b) + printRowItem(c)   
											+ printRowItem(d) + printRowItem(p1(a, b, c, d))
											+ printRowItem(p2(a, b, c, d)) 
											+ printRowItem(p3(a, b, c, d)));
	}//printRow 

	public static void printHeading()
	{
		System.out.println(" _____________________________________________________"
											+"__ ");
		System.out.println();
		System.out.println("|   a   |   b   |   c   |   d   |   p1  |   p2  |  p3 "
											+ "  |");
		System.out.println("|_______|_______|_______|_______|_______|_______|_____"
											+ "__|");
	}//printHeading

	public static void printFooter()
	{
		System.out.println("|_______|_______|_______|_______|_______|_______|_____"
											+ "__|");
	}
	public static void main(String[] args)
	{
		printHeading();//printing first lines
		boolean a = true, b = true, c = true, d = true;//initializing variables
		for (int aCount = 1; aCount <3; aCount++, a =! a)//generating a
			for (int bCount = 1; bCount <3; bCount++, b =! b)//generating b
				for (int cCount = 1; cCount <3; cCount++, c =! c)//generating c
				  for (int dCount = 1; dCount <3; dCount++, d =! d)//generating d
						printRow(a, b, c, d);
		printFooter();//printing last line
	}//main
}//class TruthTable34