public class QuadPoly
	{
		public double firstCoefficient;
		public double secondCoefficient;
		public double thirdCoefficient;

		public QuadPoly(double first, double second, double third)
		{
			firstCoefficient = first;
			secondCoefficient = second;
			thirdCoefficient = third;
		}//the constructor
		public void addQuadPoly(QuadPoly firstPoly, QuadPoly secondPoly)
		{
			firstCoefficient = firstPoly.firstCoefficient 
				+ secondPoly.firstCoefficient;
			secondCoefficient = firstPoly.secondCoefficient 
				+ secondPoly.secondCoefficient;
			thirdCoefficient = firstPoly.thirdCoefficient 
				+ secondPoly.thirdCoefficient;
		}//addQuadPoly
		public boolean equalQuadPoly(QuadPoly secondPoly)
		{
			if(firstCoefficient == secondPoly.firstCoefficient && 
				secondCoefficient == secondCoefficient &&
				thirdCoefficient == secondPoly.thirdCoefficient)
				return true;
			else
				return false;
		}//equalQuadPoly
		public boolean compareQuadPoly(QuadPoly secondPoly)
		{
			if(firstCoefficient > secondPoly.firstCoefficient)
				return true;
			else if(firstCoefficient < secondPoly.firstCoefficient)
				return false;

			if(secondCoefficient > secondPoly.secondCoefficient)
				return true;
			else if(secondCoefficient < secondPoly.secondCoefficient)
				return false;

			if(thirdCoefficient > secondPoly.thirdCoefficient)
				return true;
			return false;
		}//compareQuadPoly
		public String toString()
		{
			return firstCoefficient+"x^2+"+secondCoefficient+"x+"+thirdCoefficient;
		}//toString
	}//class
