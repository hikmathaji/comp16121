//Adding two Quadratic Polynomial and printing both two and the result. The
//coofficients of the polynomial is going to be entered by command line
	public class AddQuadPoly
	{
		public static void main(String[] args)
  	{
  		//getting the coofficients from command line 
	  	QuadPoly firstPoly = new QuadPoly(Double.parseDouble(args[0]), 
	  		Double.parseDouble(args[1]), Double.parseDouble(args[2]));
	  	QuadPoly secondPoly = new QuadPoly(Double.parseDouble(args[3]), 
	  		Double.parseDouble(args[4]), Double.parseDouble(args[5]));
	  	//creating third polynomial to hold the sum
	  	QuadPoly thirdPoly = new QuadPoly(0,0,0);
	  	//adding first two polynomial and storing into third one
	  	thirdPoly.addQuadPoly(firstPoly, secondPoly);

	  	System.out.println("Polynomial :  " + firstPoly.firstCoefficient 
	  										+ "x^2 + " + firstPoly.secondCoefficient+"x + "
	  										+ firstPoly.thirdCoefficient);
	  	System.out.println("added to   :  " + secondPoly.firstCoefficient
	  										+ "x^2 + " + secondPoly.secondCoefficient+"x + "
	  										+ secondPoly.thirdCoefficient);
	  	System.out.println("results in :  " + thirdPoly.firstCoefficient 
	  										+ "x^2 + " + thirdPoly.secondCoefficient + "x + "
	  										+ thirdPoly.thirdCoefficient);
  }//main
}//AddQuadPoly class
