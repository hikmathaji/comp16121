//Compares two given polynomial and adds them as previous programs
//the only difference is there is a toString() method

public class CompareQuadPoly
{
  public static void main(String[] args)
  {
  	//getting the coefficients from the command line
  	double firstCoefficientofFirst = Double.parseDouble(args[0]);
  	double secondCoefficientofFirst = Double.parseDouble(args[1]);
  	double thirdCoefficientofFirst = Double.parseDouble(args[2]);
  	QuadPoly firstPoly = new QuadPoly(firstCoefficientofFirst, 
  		secondCoefficientofFirst, thirdCoefficientofFirst);
  	QuadPoly secondPoly = new QuadPoly(Double.parseDouble(args[3]), 
  		Double.parseDouble(args[4]), Double.parseDouble(args[5]));
  	//doing the comparision
  	System.out.println("The Polynomial     " + firstPoly.toString());
  	if(firstPoly.equalQuadPoly(secondPoly))
  		System.out.print("is the same as:		");
  	else
  	{
  		if(firstPoly.compareQuadPoly(secondPoly))
  		{
  			System.out.print("is smaller than  ");
  		}//if
  		else
  		{
  			System.out.print("is greater than  ");
  		}//else
  	}//else
  	System.out.println(secondPoly.toString());
  }
}