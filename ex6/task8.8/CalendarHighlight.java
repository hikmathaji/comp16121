//Prints out a calendar and highlight a date on it by given command
//line arguments
public class CalendarHighlight
{
	public static void main(String[] args)
	{
		printMonth(Integer.parseInt(args[0]), Integer.parseInt(args[1]),
								Integer.parseInt(args[2]));
	} //main
	//Print the calendar for the month
	private static void printMonth(int monthStartDay, int lastDateInMonth, 
																	int highlightDate)
	{
		printMonthLineOfHypens();
		printDayNames();
		printMonthLineOfHypens();
		int weekday = 0;
		for(int monthDate = 0; monthDate < monthStartDay-1; monthDate++)
		{
			weekday++;
			printEmptyItem();
		}//for
		for(int monthDate = 1; monthDate <= lastDateInMonth; monthDate++)
		{	
			if(weekday == 7)
			{
				weekday = 0;
				System.out.println("|");
			}
			if(monthDate == highlightDate)
			{
				printHighlightedDate(monthDate);
			}//if
			else
			{
				printDate(monthDate);
			}//else
			weekday++;
		}//for
		System.out.println();
		printMonthLineOfHypens();
	} // print month

	private static void printMonthLineOfHypens()
	{
		System.out.print(" ");
		for (int dayColumnNo = 1; dayColumnNo <= 7; dayColumnNo++)
		{
			if (dayColumnNo > 1)
			System.out.print("-");
			printDateHypens();
		} //for
		System.out.println(" ");
	} //printing Month Line
		// printing the name of the day
	private static void printDayNames()
	{
		System.out.print("|");
		for (int dayColumnNo = 1; dayColumnNo <= 7; dayColumnNo++)
		{
			if (dayColumnNo > 1)
				System.out.print(" ");
			printDayName(dayColumnNo);
		} //for
		System.out.println("|");
	} // printing names of the days

	private static void printDayName(int dayNo)
	{
	switch (dayNo)
	{
		case 1: System.out.print("Su "); break;
		case 2: System.out.print("Mo "); break;
		case 3: System.out.print("Tu "); break;
		case 4: System.out.print("We "); break;
		case 5: System.out.print("Th "); break;
		case 6: System.out.print("Fr "); break;
		case 7: System.out.print("Sa "); break;
	} //switch
	} //printDayName


	private static void printDateHypens()
	{
	System.out.print("--");
	} //printDateHypens

	private static void printDate(int date)
	{
	System.out.printf(" %02d ", date);
	} //printDate
	private static void printEmptyItem()
	{
	System.out.print("    ");
	} //printDate
	private static void printHighlightedDate(int date)
	{
		System.out.printf(">%02d<", date);
	}

} //class CalendarHighlight