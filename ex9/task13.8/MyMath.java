/**
 *  The class only contains greatestCommonDivisor which finds greatest
 *  common divisor of two given numbers
 *  @author Hikmat Hajiyev
 */
public class MyMath
{
  /**
   *  Finds greates common divisor of two given numbers
   *
   *  @param multiple1OfGCD One of the numbers
   *  @param multiple2OfGCD The other number
   *  @return The GCD of multiple1OfGCD and multiple2OfGCD
   */
  public static int greatestCommonDivisor(int multiple1OfGCD,
                                          int multiple2OfGCD)
  {
     while (multiple1OfGCD != multiple2OfGCD)
      if (multiple1OfGCD > multiple2OfGCD)
        multiple1OfGCD -= multiple2OfGCD;
      else 
        multiple2OfGCD -= multiple1OfGCD;
      //returning GCD
      return multiple1OfGCD;
  } // greatestCommonDivisor
} // MyMath class
