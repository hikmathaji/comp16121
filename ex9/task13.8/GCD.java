import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *  Calculates great common divisor of three numbers and outputs the result
 *  through a graphical user interface
 *
 *  @author Hikmat Hajiyev
 */

public class GCD extends JFrame implements ActionListener
{
  private final JTextField number1TextField = new JTextField(20);
  private final JTextField number2TextField = new JTextField(20);
  private final JTextField number3TextField = new JTextField(20);
  private final JTextField resultJTextField = new JTextField(20);
  
  /**
   *  Constructor of GCD class, creats container and adds 
   *
   */
  public GCD()
  {
    setTitle("GCD");
    Container contents = getContentPane();
    contents.setLayout(new GridLayout(0, 1));
    contents.add(new JLabel("Number 1"));
    contents.add(number1TextField);
    contents.add(new JLabel("Number 2"));
    contents.add(number2TextField);
    contents.add(new JLabel("Number 3"));
    contents.add(number3TextField);
    JButton computeJButton = new JButton("Compute");
    contents.add(computeJButton);
    computeJButton.addActionListener(this);
    contents.add(new JLabel("GCD of Number 1, Number 2 and Number 3"));
    contents.add(resultJTextField);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//GCD

  public void actionPerformed(ActionEvent event)
  {
    //getting numbers from text fields
    int number1 = Integer.parseInt(number1TextField.getText());
    int number2 = Integer.parseInt(number2TextField.getText());
    int number3 = Integer.parseInt(number3TextField.getText());

    //finding gcd of first and second
    int gcdOfFirstAndSecond = MyMath.greatestCommonDivisor(number1, number2);
    //finding gcd of all
    int gcdOfAll = MyMath.greatestCommonDivisor(gcdOfFirstAndSecond, number3);
    //showing the result
    resultJTextField.setText(""+gcdOfAll);
  }//actionPerformed

  /**
   *  Main method of the GCD class
   *
   *  @param args A string array where the command line arguments are stored
   *
   */
  public static void main(String[] args)
  {
    GCD theGCD = new GCD();
    theGCD.setVisible(true);
  }//main
}//GCD class
