import java.awt.Container;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;

/**
 *  Shows the possible values that can be weighed using the numbers user 
 *  provided
 *
 *  @author Hikmat Hajiyev
 */
public class ThreeWeights extends JFrame implements ActionListener
{
  private final JTextField number1JTextField = new JTextField();
  private final JTextField number2JTextField = new JTextField();
  private final JTextField number3JTextField = new JTextField();
  private final JButton displayJButton = new JButton("Display");
  private final JTextArea displayJTextArea = new JTextArea(15,20);
    
  /**
   *  Constructor of ThreeWeights class
   *  Creates the container and adds labels
   */
  public ThreeWeights()
  {
    setTitle("ThreeWeights");
    Container contents = getContentPane();
    contents.setLayout(new BorderLayout());
    JPanel numbersPanel = new JPanel(new GridLayout(0,2));
    contents.add(numbersPanel, BorderLayout.NORTH);
    numbersPanel.add(new JLabel("First number: "));
    numbersPanel.add(number1JTextField);
    numbersPanel.add(new JLabel("Second number "));
    numbersPanel.add(number2JTextField);
    numbersPanel.add(new JLabel("Third number "));
    numbersPanel.add(number3JTextField);
    contents.add(new JScrollPane(displayJTextArea), BorderLayout.CENTER);
    contents.add(displayJButton, BorderLayout.SOUTH);
    displayJButton.addActionListener(this);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//constructor

  public void actionPerformed(ActionEvent event)
  {
    int firstNumber = Integer.parseInt(number1JTextField.getText());
    int secondNumber = Integer.parseInt(number2JTextField.getText());
    int thirdNumber = Integer.parseInt(number3JTextField.getText());
    displayJTextArea.setText("");
    for(int multiplierOfFirstNumber = -1; multiplierOfFirstNumber <= 1; 
            multiplierOfFirstNumber++)
    {
      for(int multiplierOfSecondNumber = -1; multiplierOfSecondNumber <= 1; 
            multiplierOfSecondNumber++)
      {
        for(int multiplierOfThirdNumber = -1; multiplierOfThirdNumber <= 1; 
            multiplierOfThirdNumber++)
        {
          displayJTextArea.append("" + (firstNumber * multiplierOfFirstNumber +
                                  secondNumber * multiplierOfSecondNumber +
                                  thirdNumber * multiplierOfThirdNumber));
          displayJTextArea.append("\n");
        }
      }
    }
  }//actionPerformed

 /**
   *  Main method of the Three Weights class
   *
   *  @param args A string array where the command line arguments are stored
   *
   */
  public static void main(String[] args)
  {
    new ThreeWeights().setVisible(true);
  }//main
}//StopClock class