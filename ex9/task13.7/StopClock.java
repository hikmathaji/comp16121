import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

/**
 *  A stopwatch sample that shows the started time, stopped time, split time 
 *  and elapsed time through a graphical user interface
 *
 *  @author Hikmat Hajiyev
 */
public class StopClock extends JFrame implements ActionListener
{

  
  private boolean isRunning = false;
  private long startedTime = 0;
  private long splitTime = 0;
  private long stoppedTime = 0;
  private final JLabel startTimeJLabel = new JLabel("Not started");
  private final JLabel stopTimeJLabel = new JLabel("Not started");
  private final JLabel splitTimeJLabel = new JLabel("Not started");
  private final JLabel elapsedTimeJLabel = new JLabel("Not started");
  private final JButton startStopButton = new JButton("Start / Stop");
  private final JButton splitButton = new JButton("Split");
    
  /**
   *  Constructor of StopClock class
   *  Creates the container and adds labels
   */
  public StopClock()
  {
    setTitle("Stop Clock");
    Container contents = getContentPane();
    contents.setLayout(new GridLayout(0, 2));
    contents.add(new JLabel("Started at "));
    contents.add(startTimeJLabel);
    contents.add(new JLabel("Stopped at "));
    contents.add(stopTimeJLabel);
    contents.add(new JLabel("Splitted time(seconds) "));
    contents.add(splitTimeJLabel);
    contents.add(new JLabel("Elapsed time(seconds) "));
    contents.add(elapsedTimeJLabel);
    startStopButton.addActionListener(this); 
    splitButton.addActionListener(this); 
    contents.add(startStopButton);
    contents.add(splitButton);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//constructor

  public void actionPerformed(ActionEvent event)
  {
    if(event.getSource() == startStopButton)
    {
      if(!isRunning)
      {
        startedTime = System.currentTimeMillis();
        startTimeJLabel.setText(""+startedTime);
        stopTimeJLabel.setText("Running...");
        splitTimeJLabel.setText("Running...");
        elapsedTimeJLabel.setText("Running...");
        isRunning = true;
      }//if
      else
      {
        stoppedTime = System.currentTimeMillis();
        stopTimeJLabel.setText(""+stoppedTime);
        long elapsedMilliSeconds = stoppedTime - startedTime;
        elapsedTimeJLabel.setText(""+elapsedMilliSeconds/1000);
        isRunning = false;
      }//else
      pack();
    }//if
    else if(event.getSource() == splitButton)
    {
      splitTimeJLabel.setText(""+splitTime);
      
      if(isRunning)
      {
        splitTime = System.currentTimeMillis()-startedTime;
        splitTimeJLabel.setText(""+splitTime/1000);
      }//if
    }//else if
  }//actionPerformed

 /**
   *  Main method of the Stop Clock class
   *
   *  @param args A string array where the command line arguments are stored
   *
   */
  public static void main(String[] args)
  {
    StopClock theStopClock = new StopClock();
    theStopClock.setVisible(true);
  }//main
}//StopClock class