import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

/**
 *  A stopwatch sample that shows the started time, stopped time, split time and
 *  elapsed time through a graphical user interface
 *
 *  This program has minor changes in comparision with previous StopClock
 *  @author Hikmat Hajiyev
 */
public class StopClock extends JFrame implements ActionListener
{
  private boolean isRunning = false;
  private long startedTime = 0;
  private long splitTime = 0;
  private long stoppedTime = 0;
  private final JTextField startTimeJTextField = new JTextField("Not started");
  private final JTextField stopTimeJTextField = new JTextField("Not started");
  private final JTextField splitTimeJTextField = new JTextField("Not started");
  private final JTextField elapsedTimeJTextField=new JTextField("Not started");
  private final JButton startStopButton = new JButton("Start");
  private final JButton splitButton = new JButton("Split");
    
  /**
   *  Constructor of StopClock class
   *  Creates the container and adds labels
   */
  public StopClock()
  {
    setTitle("Stop Clock");
    Container contents = getContentPane();
    contents.setLayout(new GridLayout(0, 2));
    contents.add(new JLabel("Started at "));
    contents.add(startTimeJTextField);
    contents.add(new JLabel("Stopped at "));
    contents.add(stopTimeJTextField);
    contents.add(new JLabel("Splitted time(seconds) "));
    contents.add(splitTimeJTextField);
    contents.add(new JLabel("Elapsed time(seconds) "));
    contents.add(elapsedTimeJTextField);

    startStopButton.addActionListener(this); 

    splitButton.addActionListener(this); 

    contents.add(startStopButton);
    contents.add(splitButton);
    splitButton.setEnabled(false);
    startTimeJTextField.setEnabled(false);
    stopTimeJTextField.setEnabled(false);
    splitTimeJTextField.setEnabled(false);
    elapsedTimeJTextField.setEnabled(false);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//constructor

  public void actionPerformed(ActionEvent event)
  {
    int asdf = 10;
    System.out.println(++asdf);
    if (event.getSource() == startStopButton)
    {
      if (!isRunning)
      {
        startedTime = System.currentTimeMillis();
        startTimeJTextField.setText("" + startedTime);
        stopTimeJTextField.setText("Running...");
        splitTimeJTextField.setText("Running...");
        elapsedTimeJTextField.setText("Running...");
        splitButton.setEnabled(true);
        startStopButton.setText("Stop");
        isRunning = true;
      }//if
      else
      {
        stoppedTime = System.currentTimeMillis();
        stopTimeJTextField.setText("" + stoppedTime);
        long elapsedMilliSeconds = stoppedTime - startedTime;
        elapsedTimeJTextField.setText("" + elapsedMilliSeconds / 1000);
        isRunning = false;
        splitButton.setEnabled(false);
        startStopButton.setText("Start");
      }//else
      pack();
    }//if
    else if (event.getSource() == splitButton)
    {
      splitTimeJTextField.setText("" + splitTime);
      
      if (isRunning)
      {
        splitTime = System.currentTimeMillis() - startedTime;
        splitTimeJTextField.setText("" + splitTime / 1000);
      }//if
    }//else if
  }//actionPerformed

 /**
   *  Main method of the Stop Clock class
   *
   *  @param args A string array where the command line arguments are stored
   *
   */
  public static void main(String[] args)
  {
    StopClock theStopClock = new StopClock();
    theStopClock.setVisible(true);
  }//main
}//StopClock class