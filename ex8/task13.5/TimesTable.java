/**
 *  Prints times table in a grid view
 *  Boundries are given by command line arguments
 *
 *  @author Hikmat Hajiyev
 */

import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class TimesTable extends JFrame
{

  /**
   *  Constructor of TimesTable class
   *  Creates the container and adds labels
   */
  public TimesTable(int first, int second)
  {
    setTitle("Hello Family");
    Container contents = getContentPane();
    contents.setLayout(new GridLayout(0, 5, 20, 10));
    for(int index = 1; index <= second; index++)
    {
      contents.add(new JLabel(""+index));
      contents.add(new JLabel("x"));
      contents.add(new JLabel(""+first));
      contents.add(new JLabel("="));
      int multiplication = index * first;
      contents.add(new JLabel(""+multiplication));
    }
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//constructor

 /**
   *  Main method of the TimesTable class
   *
   *  @param args A string array where the command line arguments are stored
   *
   */
  public static void main(String[] args)
  {
    int firstNumber = Integer.parseInt(args[0]);
    int secondNumber = Integer.parseInt(args[1]);
    TimesTable theTimesTable = new TimesTable(firstNumber, secondNumber);
    theTimesTable.setVisible(true);
  }//main
}//HelloFamily class