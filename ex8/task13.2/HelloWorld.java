/**
 *  just says hello world
 *
 *  @author Hikmat Hajiyev
 */


import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloWorld extends JFrame
{
  /**
   *  Constructor of HelloWorld class
   *  Creates the container and adds labels
   */
  public HelloWorld()
  {
    setTitle("Hello World");
    Container contents = getContentPane();
    contents.add(new JLabel("Salam Dunya"));
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }

 /**
   *  Main method of the HelloFamily class
   *
   *  @param args A string array where the command line arguments are stored
   *
   */
  public static void main(String[] args)
  {
    HelloWorld theHelloWorld = new HelloWorld();
    theHelloWorld.setVisible(true);
  }//main
}//StudentsCalling