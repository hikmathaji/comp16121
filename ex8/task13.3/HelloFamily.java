/**
 *  Greets the whole family members in alphabetical order
 *
 *  @author Hikmat Hajiyev
 */

import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloFamily extends JFrame
{

  /**
   *  Constructor of HelloFamily class
   *  Creates the container and adds labels
   */
  public HelloFamily()
  {
    setTitle("Hello World");
    Container contents = getContentPane();
    contents.setLayout(new FlowLayout());
    contents.add(new JLabel("Salam Afat"));
    contents.add(new JLabel("Salam Almaz"));
    contents.add(new JLabel("Salam Goncha"));
    contents.add(new JLabel("Salam Hikmat"));
    contents.add(new JLabel("Salam Farida"));
    contents.add(new JLabel("Salam Fariz"));
    contents.add(new JLabel("Salam Fidan"));
    contents.add(new JLabel("Salam Fikrat"));
    contents.add(new JLabel("Salam Firudun"));
    contents.add(new JLabel("Salam Togrul"));
    contents.add(new JLabel("Salam Tural"));
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    pack();
  }//Hello Family constructor

 /**
   *  Main method of the HelloFamily class
   *
   *  @param args A string array where the command line arguments are stored
   *
   */
  public static void main(String[] args)
  {
    HelloFamily theHelloFamily = new HelloFamily();
    theHelloFamily.setVisible(true);
  }//main
}//HelloFamily class