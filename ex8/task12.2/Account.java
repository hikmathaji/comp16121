/**
 *  This class is part of StudentsCalling exercise
 *  Represents the accounts of phones of the students
 *
 *  @author Hikmat Hajiyev
 */
public class Account
{
  private final String name;
  private int balance;

  /**
   *  Constructor of Account class
   *  Set balance to zero
   *  @param newName A name of the account
   */
  public Account(String newName)
  {
    name = newName;
    balance = 0;
  }//constructor

  /**
   *  Represents toping up the balance
   *  Balance is increased by the amount of 100*value
   *  @param amount Amount by pounds to be topped up
   */
  public void topUp(int amount)
  {
    balance += amount*100;
  }//topUp

  /**
   *  Gets the current balance
   *  @return The current balance
   */
  public int getBalance()
  {
    return balance;
  }//getBalance

  /**
   *  Represents the use of account
   *  
   *  @param value A value equal to the duration to be subtracted from balance
   */
  public void use(int value)
  {
    balance-=value;
  }//use

  /**
   *  Converts account information to string
   *  
   *  @return  Account information, name and balance
   */
  public String toString()
  {
    return "Account (" + name + ", " + balance + ")";
  }//toString

}//Account