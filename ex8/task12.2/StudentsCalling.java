/**
 *  Stimulates simple scenario in which students purchase 
 *  and use mobile phones
 *
 *  @author Hikmat Hajiyev
 */

public class StudentsCalling
{
  /**
   *  Represents the creation of the new Student object
   *  Creates the student object and prints it.
   *  
   *  @param  studentName The name of the student to be created
   *  @return The student object
   */
  private static Student create(String studentName)
  {
    System.out.println("Creating student " + studentName);
    Student student = new Student(studentName);
    System.out.println("Result:"); 
    System.out.println(student.toString());
    return student;
  }//create

  /**
   *  Represents the creation of the new Student object
   *  Discards the old phone if student already has one
   *  
   *  @param  student Student who is buying a new phone
   *  @param  phoneName The name of the new phone
   *  @param  accountName The name of the new account of the phone
   *
   */
  private static void buyPhone(Student student, String phoneName, 
                               String accountName)
  {
    if(student.hasPhone() == true)
    {
      System.out.println("Now let us discard a phone.");
    }//if
    System.out.println(student.toString());
    System.out.println("is buying a phone " + phoneName);
    System.out.println("with account " + accountName);
    student.buyPhone(phoneName, accountName);
    System.out.println("Result:");
    System.out.println(student.toString());
  }//buyPhone
 /**
   *  Represents call made by student
   *  Prints the has no effect text when student has no phone
   *  Makes the call and prints the student information
   *
   *  @param  student Student who is buying a new phone
   *  @param  duration Duration of the call
   *
   */
  private static void call(Student student, int duration)
  {
    if (student.hasPhone() == false)
    {
      System.out.println("This next call has no effect, as has no phone!");
    }//if
    System.out.println(student.toString());
    System.out.println("is making a call for desired " + duration + "seconds");
    student.call(duration);
    System.out.println("Result:");
    System.out.println(student.toString());
  }//call
 /**
   *  Represents top up made by student
   *  Prints the has no effect text when student has no phone
   *  Makes the top up and prints the student information
   *
   *  @param  student Student who is buying a new phone
   *  @param  amount Amount of the top up.
   *
   */
  private static void topUp(Student student, int amount)
  {
    if (student.hasPhone() == false)
    {
      System.out.println("This next top up has no effect, as has no phone!");
    }//if
    System.out.println(student.toString());
    System.out.println("is topping up by " + amount);
    student.topUp(amount);
    System.out.println("Result:");
    System.out.println(student.toString());
  }//topUp

 /**
   *  Main method of the StudentsCalling class
   *
   *  @param args A string array where the comman line arguments are stored
   *
   */
  public static void main(String[] args)
  {
    //creating myself
    Student hikmat = create("Hikmat Hajiyev");
    //creating a friend
    Student hasan = create("Hasan Hasanzada");
    //creating another friend
    Student elnur = create("Elnur Mammadli");
    //me calling someone for 5 minutes
    call(hikmat, 300);
    //oops... forgot to buy a phone
    buyPhone(hikmat, "Huawei", "giffgaff");
    //hasan also buys a phone
    buyPhone(hasan, "Nexus5", "lycamobile");
    //but elnur wants to buy something cool
    buyPhone(elnur, "iPhone6", "lycamobile");
    //now we can talk to each other, calling hasan
    call(hikmat, 300);
    //oops... forgot to top up
    topUp(hikmat, 10);
    //now calling again
    call(hikmat, 300);
    //hasan doesn't forget to top up
    topUp(hasan, 10);
    //elnur also does but he is a rich boy
    topUp(elnur, 200);
    //elnur tells me how awesome phone he has for hours
    call(elnur, 10000);
    //I'm so impressed that I also decide to buy an iPhone
    buyPhone(hikmat, "iPhone6", "giffgaff");


  }//main
}//StudentsCalling