/**
 *  This class is part of StudentsCalling exercise
 *  Represents the students
 *
 *  @author Hikmat Hajiyev
 */
public class Student
{
	private final String name;
  private Phone phone;
  /**
   *  Constructor of Student class
   *  Set the phone to null
   *  @param newName Name of the student
   */
  public Student(String newName)
  {
    name = newName;
    phone = null;
  }//constructor
  /**
   *  Checks if student has a phone or not
   *  @return true if student has a phone
   */
  public boolean hasPhone()
  {
    return phone == null ? false : true;
  }//hasPhone

  /**
   *  Represents buying a new phone by a student
   *  @param phoneName The name of the new phone
   *  @param accountName The name of the phone's account
   */
  public void buyPhone(String phoneName, String accountName)
  {
    phone = new Phone(phoneName, accountName);
  }//buyPhone

  /**
   *  Converts Phone information to string
   *  
   *  @return  Phone information, name and balance
   */
  public String toString()
  {
    String res = "Student(" + name + ", ";
    if(this.hasPhone())
      res+=phone.toString();
    else
      res+="null";
    res += ")";
    return res;
  }//toString

  /**
   *  Represents the student calling
   *  @param duration The duration of desired call
   */
  public void call(int duration)
  {
    if (this.hasPhone())
    {
      phone.use(duration);
    }
  }//call
  
  /**
   *  Represents toping up the balance
   *  @param value The amount to top up the account
   */
  public void topUp(int value)
  { 
    phone.topUp(value);
  }//topUp

}//Student