/**
 *  This class is part of StudentsCalling exercise
 *  Represents the phones of the students
 *
 *  @author Hikmat Hajiyev
 */
public class Phone
{
  private final String name;
  private int totalCallDuration;
  private Account account;

  /**
   *  Constructor of Phone class
   *  Set total call duration to zero
   *  @param newName A name of the phone
   *  @param accountName A name of the account
   */
  public Phone(String newName, String accountName)
  {
    name = newName;
    totalCallDuration = 0;
    account = new Account(accountName);
  }//constructor

  /**
   *  Represents the using of phone
   *  Balance is decreased by duration
   *  Total call duration is increased by duration
   *  @param duration The duration of desired call
   */
  public void use(int duration)
  {
    if(account.getBalance() >= duration)
    { 
      totalCallDuration += duration;
      account.use(duration);
    }
  }//use

  /**
   *  Represents toping up the balance
   *  @param value The amount to top up the account
   */
  public void topUp(int value)
  {
    account.topUp(value);
  }
  
  /**
   *  Converts Phone information to string
   *  
   *  @return  Phone information, name and balance
   */
  public String toString()
  {
    return "Phone (" + name + ", " + totalCallDuration + ", " + account.toString() + ")";
  }//toString

}//Phone 