//part of student calling exercise, implements accounts of 
//phones of students
public class Account
{
  private final String name;
  private int balance;

  public Account(String newName)
  {
    name = newName;
    balance = 0;
  }//constructor

  public void topUp(int value)
  {
    balance += value*100;
  }//topUp

  public int getBalance()
  {
    return balance;
  }//getBalance

  public void use(int value)
  {
    balance-=value;
  }//use

  public String toString()
  {
    return "Account (" + name + ", " + balance + ")";
  }//toString

}//Account