//part of students calling exercise, implements students
public class Student
{
	private final String name;
  private Phone phone;

  public Student(String newName)
  {
    name = newName;
    phone = null;
  }//constructor

  public boolean hasPhone()
  {
    return phone == null ? false : true;
  }//hasPhone

  public void buyPhone(String phoneName, String accountName)
  {
    phone = new Phone(phoneName, accountName);
  }//buyPhone

  public String toString()
  {
    String res = "Student(" + name + ", ";
    if(this.hasPhone())
      res+=phone.toString();
    else
      res+="null";
    res += ")";
    return res;
  }//toString

  public void call(int duration)
  {
    if (this.hasPhone())
    {
      phone.use(duration);
    }
  }//call

  public void topUp(int value)
  { 
    phone.topUp(value);
  }//topUp

}//Student