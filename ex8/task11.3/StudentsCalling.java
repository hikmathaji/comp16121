//stimulates simple scenario in which students purchase and use mobile phones
public class StudentsCalling
{
  private static Student create(String studentName)
  {
    System.out.println("Creating student " + studentName);
    Student student = new Student(studentName);
    System.out.println("Result:"); 
    System.out.println(student.toString());
    return student;
  }//create
  
  private static void buyPhone(Student student, String phoneName, 
                               String accountName)
  {
    if(student.hasPhone() == true)
    {
      System.out.println("Now let us discard a phone.");
    }//if
    System.out.println(student.toString());
    System.out.println("is buying a phone " + phoneName);
    System.out.println("with account " + accountName);
    student.buyPhone(phoneName, accountName);
    System.out.println("Result:");
    System.out.println(student.toString());
  }//buyPhone

  private static void call(Student student, int duration)
  {
    if (student.hasPhone() == false)
    {
      System.out.println("This next call has no effect, as has no phone!");
    }//if
    System.out.println(student.toString());
    System.out.println("is making a call for desired " + duration + "seconds");
    student.call(duration);
    System.out.println("Result:");
    System.out.println(student.toString());
  }//call

  private static void topUp(Student student, int amount)
  {
    if (student.hasPhone() == false)
    {
      System.out.println("This next top up has no effect, as has no phone!");
    }//if
    System.out.println(student.toString());
    System.out.println("is topping up by " + amount);
    student.topUp(amount);
    System.out.println("Result:");
    System.out.println(student.toString());
  }//topUp

  public static void main(String[] args)
  {
    //creating myself
    Student hikmat = create("Hikmat Hajiyev");
    //creating a friend
    Student hasan = create("Hasan Hasanzada");
    //creating another friend
    Student elnur = create("Elnur Mammadli");
    //me calling someone for 5 minutes
    call(hikmat, 300);
    //oops... forgot to buy a phone
    buyPhone(hikmat, "Huawei", "giffgaff");
    //hasan also buys a phone
    buyPhone(hasan, "Nexus5", "lycamobile");
    //but elnur wants to buy something cool
    buyPhone(elnur, "iPhone6", "lycamobile");
    //now we can talk to each other, calling hasan
    call(hikmat, 300);
    //oops... forgot to top up
    topUp(hikmat, 10);
    //now calling again
    call(hikmat, 300);
    //hasan doesn't forget to top up
    topUp(hasan, 10);
    //elnur also does but he is a rich boy
    topUp(elnur, 200);
    //elnur tells me how awesome phone he has for hours
    call(elnur, 10000);
    //I'm so impressed that I also decide to buy an iPhone
    buyPhone(hikmat, "iPhone6", "giffgaff");


  }//main
}//StudentsCalling