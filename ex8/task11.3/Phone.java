//part of students calling, implements phones of students
public class Phone
{
  private final String name;
  private int totalCallDuration;
  private Account account;

  public Phone(String newName, String accountName)
  {
    name = newName;
    totalCallDuration = 0;
    account = new Account(accountName);
  }//constructor

  public void use(int duration)
  {
    if(account.getBalance() >= duration)
    { 
      totalCallDuration += duration;
      account.use(duration);
    }
  }//use

  public void topUp(int value)
  {
    account.topUp(value);
  }

  public String toString()
  {
    return "Phone (" + name + ", " + totalCallDuration + ", " + account.toString() + ")";
  }//toString

}//Phone 